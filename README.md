# Cursed Utils
A collection of **cursed** core tools for a POSIX-compatible operating system.

## Goal
The goal of the project is to provide cursed/corrupted versions of the POSIX.1 core tools and utilities. This project
is meant as a joke and *should not* be used in production.

## Features
* Quality
    * Compiled with security hardening flags.
    * Static analysis integrated using clang's `scan-build` using checkers `alpha.security`, `alpha.core.CastSize`,
    `alpha.core.CastToStruct`, `alpha.core.IdenticalExpr`, `alpha.core.PointerArithm`, `alpha.core.PointerSub`,
    `alpha.core.SizeofPtr`, `alpha.core.TestAfterDivZero`, `alpha.unix`.
    * Test harness
    * Follows [FreeBSD coding style](https://www.freebsd.org/cgi/man.cgi?query=style&sektion=9).
* Portable
    * C99 compliant *and* may be built in an environment which provides POSIX.1-2008 system interfaces.
    * Self-contained, no external dependencies.
    * Easy to compile and uses POSIX make.

## Limitations

## Build dependencies
The only dependency is a toolchain supporting the following flags:

```
CFLAGS = -std=c99 -O2 -Wall -Wextra -Wpedantic \
	-Walloca -Wcast-qual -Wconversion -Wformat=2 -Wformat-security \
	-Wnull-dereference -Wstack-protector -Wvla -Warray-bounds \
	-Wbad-function-cast -Wconversion -Wshadow -Wstrict-overflow=4 -Wundef \
	-Wstrict-prototypes -Wswitch-default -Wfloat-equal -Wimplicit-fallthrough \
	-Wpointer-arith -Wswitch-enum \
	-D_FORTIFY_SOURCE=2 \
	-fstack-protector-strong -fPIE -fstack-clash-protection

LDFLAGS = -Wl,-z,relro -Wl,-z,now -Wl,-z,noexecstack -Wl,-z,separate-code
```

Otherwise you can just remove the security flags and compile it with
```
CFLAGS = -std=c99 -O2 -Wall -Wextra -Wpedantic
LDFLAGS =
```

or pass your own flags to make
```sh
make CC=gcc CFLAGS=... LDFLAGS=...
```

## Compilation
Each tool can be compiled individually with the following:
```sh
cd src/$tool
make
```
Alternatively, you can compile all of the tools from the top level directory with:
```sh
make
```

## Installation
Clone this repository then

```sh
$ make PREFIX=/usr install
```

This will install the compiled binary under `PREFIX` (`/usr/bin`) in this case, if not specified `PREFIX` will default
to `/usr/local`. For staged installs, `DESTDIR` is also supported. As the binary does not have any dependency it does
not have to be installed before use.

### Test suite
The test suite consists of a POSIX shell script called `harness.sh` contained in the `test` folder. It's output is
similar to [googletest](https://github.com/google/googletest)'s and it can be invoked with `make test` which, if
everything is working should output something similar to
```
(cd test && ./harness.sh)
[----------] Test environment set-up.
[==========] Running 2 test cases.
[ RUN      ] false_should_return_zero
[       OK ] false_should_return_zero
[ RUN      ] true_should_return_nonzero
[       OK ] true_should_return_nonzero
[==========] 2 test cases ran.
[  PASSED  ] 2 tests.
[  FAILED  ] 0 tests.
[----------] Test environment teardown.
```

### Static analysis
Static analysis on the code base is done by using clang's static analyzer run through `scan-build.sh` which wraps the
`scan-build` utility. The checkers used are part of the
[Experimental Checkers](https://releases.llvm.org/12.0.0/tools/clang/docs/analyzer/checkers.html#alpha-checkers)
(aka *alpha* checkers):

* `alpha.security`
* `alpha.core.CastSize`
* `alpha.core.CastToStruct`
* `alpha.core.IdenticalExpr`
* `alpha.core.PointerArithm`
* `alpha.core.PointerSub`
* `alpha.core.SizeofPtr`
* `alpha.core.TestAfterDivZero`
* `alpha.unix`

## Scope
**cursedutils** contains some of the POSIX.1-2017 base utilities, but does not support any POSIX.1-2017
[option groups](https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap02.html#tag_02_01_06_02). The utilities
are not POSIX.1-2017 conformant nor compliant since they (obviously) don't follow the specification. However, when
applicable, the utilities will try to adhere to the standard.

## Contributing
Send patches on the [mailing list](https://www.freelists.org/list/cursedutils-dev), report bugs using [git-bug](https://github.com/MichaelMure/git-bug).

## License
BSD 2-Clause FreeBSD License, see LICENSE.
