.PHONY: all clean test

include config.mk

SRCS != find src -type f -regex ".*\.c"
OBJS = ${SRCS:.c=.o}
PROGS = ${OBJS:.o=}

all: ${PROGS}

test: ${PROGS}
	(cd test && ./harness.sh)

clean:
	${RM} -f ${OBJS} ${PROGS}
