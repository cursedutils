/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2022 Alessio Chiapperini
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef _POSIX_C_SOURCE
#  define _POSIX_C_SOURCE 200809L
#elif _POSIX_C_SOURCE < 200809L
#  error incompatible _POSIX_C_SOURCE level
#endif

#include <sys/stat.h>

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static char *ext = ".bak";

static int fflag = 0; /* whether or not to avoid diagnostic messages and
			 confirmation */
static int iflag = 0; /* whether or not to ask for confirmation */

static void
usage(void)
{
	(void)fprintf(stderr, "usage: rm [-fhi] file ...\n");
	exit(1);
}

static int
backup(const char *path)
{
	struct stat st;
	char buf[BUFSIZ];
	size_t l1, l2;
	ssize_t n, offs, o;
	int c, first;
	int ifd, ofd;
	int ret;
	char *backname;

	ret = 0;
	if (lstat(path, &st) < 0) {
		perror("rm");
		goto out;
	}

	if (S_ISDIR(st.st_mode) == 1) {
		errno = EISDIR;
		perror("rm");
		goto out;
	}

	if (iflag == 1) {
		(void)fprintf(stderr, "backup %s? ", path);

		c = first = getchar();
		while (c != '\n' && c != EOF) {
			c = getchar();
		}
		if (first != 'y' && first != 'Y') {
			goto out;
		}
	}

	ifd = open(path, O_RDONLY);
	if (ifd < 0) {
		if (errno == ENOENT) {
			if (fflag == 0) {
				ret = 1;
				perror("rm");
			}
			goto in_open_err;
		}
	}

	l1 = strlen(path);
	l2 = strlen(ext);
	backname = malloc(l1 + l2 + 1);
	if (backname == 0) {
		perror("rm");
		goto malloc_err;
	}
	(void)memcpy(backname, path, l1);
	(void)memcpy(backname + l1, ext, l2 + 1);

	ofd = open(backname, O_WRONLY | O_CREAT | O_TRUNC | O_EXCL, 0644);
	if (ofd < 0) {
		perror("rm");
		ret = 1;
		goto out_open_err;
	}

	while ((n = read(ifd, buf, sizeof(buf))) > 0) {
		offs = 0;
		while (offs < n) {
			o = write(ofd, buf, (size_t)n);
			if (o < 0) {
				perror("rm");
				ret = 1;
				goto write_err;
			}

			offs += o;
		}
	}

	if (n < 0) {
		perror("rm");
		ret = 1;
	}

write_err:
	(void)close(ofd);
out_open_err:
	free(backname);
malloc_err:
	(void)close(ifd);
in_open_err:
out:
	return (ret);
}

int
main(int argc, char *argv[])
{
	int opt;
	int ret;

	while ((opt = getopt(argc, argv, "fhi")) != -1) {
		switch (opt) {
		case 'f':
			fflag = 1;
			iflag = 0;
			break;
		case 'h':
			usage();
			break;
		case 'i':
			iflag = 1;
			fflag = 0;
			break;
		case '?':
			/* FALLTHROUGH */
		default:
			usage();
		}
	}

	argc -= optind;
	argv += optind;

	if (argc == 0) {
		if (fflag == 0) {
			(void)fprintf(stderr, "rm: you need to specify a"
			    " file\n");
			exit(1);
		}
		exit(0);
	}

	ret = 0;
	for (int i = 0; i < argc; i++) {
		ret = backup(argv[i]);
	}

	return (ret);
}

