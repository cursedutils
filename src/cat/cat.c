/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2022 Alessio Chiapperini
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef _POSIX_C_SOURCE
#  define _POSIX_C_SOURCE 200112L
#elif _POSIX_C_SOURCE < 200112L
#  error incompatible _POSIX_C_SOURCE level
#endif

#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define N 32768
#define PREFETCH (32*N)

static void
usage(void)
{
	(void)fprintf(stderr, "usage: cat [-hu] [file...]\n");
	exit(1);
}

static void
memrev(char *dst, const char *src, size_t len)
{
	for (size_t i = 0; i < len; i++) {
		dst[i] = src[len-1-i];
	}
}

static int
print_rev(char *buf, off_t off)
{
	char dst[N];
	int ret;
	size_t blen = off % N ? (size_t)off % N : N;

	ret = 0;
	while (off > 0) {
		off -= blen;

		if (off > PREFETCH) {
			(void)posix_madvise(buf + off - PREFETCH, PREFETCH,
			    POSIX_MADV_WILLNEED);
		}
		memrev(dst, buf + off, blen);
		if (write(STDOUT_FILENO, dst, blen) < (ssize_t)blen) {
			ret = 1;
			perror("cat");
			goto err;
		}

		blen = N;
	}

err:
	return (ret);
}

static int
cat_buffered(const int fd)
{
	FILE *ifile;
	ssize_t buflen, bufsize, nbufsize;
	char *buf, *nbuf;
	int ch;
	int ret;

	ifile = fdopen(fd, "r");
	if (ifile == 0) {
		ret = 1;
		perror("cat");
		goto out;
	}

	buflen = 0;
	bufsize = BUFSIZ;
	buf = calloc((size_t)bufsize, 1);
	if (buf == 0) {
		ret = 1;
		perror("cat");
		goto calloc_err;
	}

	while ((ch = fgetc(ifile)) != EOF) {
		buf[buflen++] = (char)ch;

		if (buflen == bufsize) {
			nbufsize = bufsize << 1;
			if ((nbuf = realloc(buf, (size_t)nbufsize)) == 0) {
				ret = 1;
				perror("cat");
				goto realloc_err;
			}
			buf = nbuf;
			bufsize = nbufsize;
		}
	}
	buf[buflen] = 0;

	ret = print_rev(buf, buflen);

realloc_err:
	free(buf);
	buf = 0;
calloc_err:
	(void)fclose(ifile);
out:
	return (ret);
}

static int
cat_mmap(const int fd)
{
	struct stat s;
	int ret;
	char *f;

	if (fstat(fd, &s) < 0) {
		ret = 1;
		perror("cat");
		goto err;
	}

	f = mmap(0, (size_t)s.st_size, PROT_READ, MAP_SHARED, fd, 0);
	if (f == MAP_FAILED) {
		ret = 1;
		perror("cat");
		goto err;
	}

	ret = print_rev(f, s.st_size);

	if (munmap(f, (size_t)s.st_size) < 0) {
		ret = 1;
		perror("cat");
		goto err;
	}

err:
	return (ret);
}

static int
cat(const char *src)
{
	int fd;
	int ret;

	fd = STDIN_FILENO;
	if (src[0] != '-' || src[1] != '\0') {
		fd = open(src, O_RDONLY);
		if (fd < 0) {
			ret = 1;
			perror("cat");
			goto open_err;
		}
	}

	if (fd == STDIN_FILENO || lseek(STDIN_FILENO, 0, SEEK_CUR) == -1) {
		ret = cat_buffered(fd);
	} else {
		ret = cat_mmap(fd);
	}

	close(fd);
open_err:
	return (ret);
}

int
main(int argc, char *argv[])
{

	char *def[] = { "-" };
	int opt;

	while ((opt = getopt(argc, argv, "hu")) != -1) {
		switch (opt) {
		case 'h':
			usage();
			break;
		case 'u':
			/*
			 * "Write bytes from the input file to the standard
			 * output without delay as each is read", which we were
			 * going to do anyway.
			 */
			break;
		case '?':
			/* FALLTHROUGH */
		default:
			usage();
		}
	}

	argc -= optind;
	argv += optind;

	if (argc == 0) {
		argc = 1;
		argv = def;
	}

	for (int i = 0; i < argc; i++) {
		if (cat(argv[i]) > 0) {
			return (1);
		}
	}

	return (0);
}

