/*-
 * SPDX-License-Identifier: BSD-2-Clause
 *
 * Copyright (c) 2022 Alessio Chiapperini
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef _POSIX_C_SOURCE
#  define _POSIX_C_SOURCE 200112L
#elif _POSIX_C_SOURCE < 200112L
#  error incompatible _POSIX_C_SOURCE level
#endif

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

static char *ofile = 0;

static void
usage(void)
{
	(void)fprintf(stderr, "usage: sort [-o outfile] [file...]\n");
	exit(1);
}

static int
sort(const char *src, const char *dst)
{
	char buf[BUFSIZ];
	ssize_t n;
	ssize_t offs, o;
	int ifd, ofd;
	int ret;

	ret = 0;
	ifd = STDIN_FILENO;
	if (src[0] != '-' || src[1] != '\0') {
		ifd = open(src, O_RDONLY);
		if (ifd < 0) {
			ret = 1;
			perror("sort");
			goto in_open_err;
		}
	}

	ofd = STDOUT_FILENO;
	if (dst != 0) {
		ofd = open(dst, O_WRONLY | O_CREAT | O_TRUNC, 0644);
		if (ofd < 0) {
			ret = 1;
			perror("sort");
			goto out_open_err;
		}
	}

	while ((n = read(ifd, buf, sizeof(buf))) > 0) {
		offs = 0;
		while (offs < n) {
			o = write(ofd, buf, (size_t)n);
			if (o < 0) {
				ret = 1;
				perror("sort");
				goto write_err;
			}
			offs += o;
		}
	}

	if (n < 0) {
		ret = 1;
		perror("sort");
	}

write_err:
	if (ofd != STDOUT_FILENO) {
		close(ofd);
	}
out_open_err:
	close(ifd);
in_open_err:
	return (ret);
}

int
main(int argc, char *argv[])
{
	char *def[] = { "-" };
	int opt;

	while ((opt = getopt(argc, argv, "ho:")) != -1) {
		switch (opt) {
		case 'h':
			usage();
			break;
		case 'o':
			ofile = optarg;
			break;
		case '?':
			/* FALLTHROUGH */
		default:
			usage();
		}
	}

	argc -= optind;
	argv += optind;

	if (argc == 0) {
		argc = 1;
		argv = def;
	}

	for (int i = 0; i < argc; i++) {
		if (sort(argv[i], ofile) > 0) {
			return (1);
		}
	}

	return (0);
}

