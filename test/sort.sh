echo -n "this is a test file" > "$TMPDIR"/test-file-1
echo -n "this is another test file" > "$TMPDIR"/test-file-2
echo -n "dashed" > ${TMPDIR}/-dashed

sort_should_handle_one_file() {
    tool=`find ../src -type f -name "sort"`
    ct="$(${tool} ${TMPDIR}/test-file-1)"
    ref=$(cat ${TMPDIR}/test-file-1)

    [ "$ct" = "$ref" ]
}

sort_should_handle_two_files() {
    tool=`find ../src -type f -name "sort"`
    ct="$(${tool} ${TMPDIR}/test-file-1 ${TMPDIR}/test-file-2)"
    ref1=$(cat ${TMPDIR}/test-file-1)
    ref2=$(cat ${TMPDIR}/test-file-2)
    ref=$(echo -n "$ref1$ref2")

    [ "$ct" = "$ref" ]
}

sort_should_handle_stdin() {
    tool=`find ../src -type f -name "sort"`
    ct="$(echo -n "this is from stdin" | ${tool} -)"
    ref=$(echo -n "this is from stdin")
    [ "$ct" = "$ref" ] || return 1
    ct="$(echo -n "this is from stdin" | ${tool})"
    [ "$ct" = "$ref" ]
}

sort_should_handle_o_flag() {
    # The default behavior is to flush at every write
    $(echo -n "this is from stdin" | ${tool} -o ${TMPDIR}/out)
    ct=$(cat ${TMPDIR}/out)
    [ "$ct" = "this is from stdin" ]
}

sort_should_handle_dash() {
    tool=`find ../src -type f -name "sort"`
    ct="$(${tool} -- ${TMPDIR}/-dashed)"
    ref=$(echo -n "dashed")
    [ "$ct" = "$ref" ]
}
