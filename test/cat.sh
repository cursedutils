echo -n "this is a test file" > "$TMPDIR"/test-file-1
echo -n "this is another test file" > "$TMPDIR"/test-file-2
echo -n "dashed" > ${TMPDIR}/-dashed

cat_should_handle_one_file() {
    tool=`find ../src -type f -name "cat"`
    ct="$(${tool} ${TMPDIR}/test-file-1)"
    ref=$(rev ${TMPDIR}/test-file-1)

    [ "$ct" = "$ref" ]
}

cat_should_handle_two_files() {
    tool=`find ../src -type f -name "cat"`
    ct="$(${tool} ${TMPDIR}/test-file-1 ${TMPDIR}/test-file-2)"
    ref1=$(rev ${TMPDIR}/test-file-1)
    ref2=$(rev ${TMPDIR}/test-file-2)
    ref=$(echo -n "$ref1$ref2")

    [ "$ct" = "$ref" ]
}

cat_should_handle_stdin() {
    tool=`find ../src -type f -name "cat"`
    ct="$(echo -n "this is from stdin" | ${tool} -)"
    ref=$(echo -n "this is from stdin" | rev)
    [ "$ct" = "$ref" ] || return 1
    ct="$(echo -n "this is from stdin" | ${tool})"
    [ "$ct" = "$ref" ]
}

cat_should_handle_u_flag() {
    # The default behavior is to flush at every write
    ct="$(echo -n "this is from stdin" | ${tool} -u)"
    ref=$(echo -n "this is from stdin" | rev)
    [ "$ct" = "$ref" ]
}

cat_should_handle_dash() {
    tool=`find ../src -type f -name "cat"`
    ct="$(${tool} -- ${TMPDIR}/-dashed)"
    ref=$(echo -n "dashed" | rev)
    [ "$ct" = "$ref" ]
}
