rm_backup_file() {
    tool=`find ../src -type f -name "rm"`
    ${tool} ${TMPDIR}/simple
    diff ${TMPDIR}/simple ${TMPDIR}/simple.bak
}

rm_should_handle_dash() {
    tool=`find ../src -type f -name "rm"`
    ${tool} -- ${TMPDIR}/-dashed
    [ -e ${TMPDIR}/-dashed -a -e ${TMPDIR}/-dashed.bak ]
}

rm_should_handle_directory() {
    tool=`find ../src -type f -name "rm"`
    ${tool} ${TMPDIR}/ 2> /dev/null
    [ $? = 0 -a ! -f ${TMPDIR}/.bak ]
}

rm_should_follow_symlinks() {
    tool=`find ../src -type f -name "rm"`
    ${tool} ${TMPDIR}/lnk
    diff ${TMPDIR}/orig ${TMPDIR}/lnk.bak
}
