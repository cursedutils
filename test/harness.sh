#!/bin/sh
set -eu

TMPDIR="."
TMPDIR=$TMPDIR/cursedutils-tests
mkdir -p "$TMPDIR"

setup() {
    # Dashed file
    touch -- ${TMPDIR}/-dashed

    # Simple file
    printf "foo\nbar" > ${TMPDIR}/simple

    # Symbolic link
    echo "orig" > ${TMPDIR}/orig
    $(cd ${TMPDIR} && ln -s orig lnk)

    printf "[----------] Test environment set-up.\n"
}

teardown() {
    rm -rf "$TMPDIR"
    printf "[----------] Test environment teardown.\n"
}

trap teardown EXIT

npass=0
nfail=0

run_tests() {
    printf "[==========] Running %d test cases.\n" "$#"

    for t in "$@"
    do
        printf "[ %-8s ] %s\n" "RUN" "$t"

        if "$t"
        then
            printf "[ %8s ] %s\n" "OK" "$t"
            npass=$((npass+1))
        else
            printf "[ %8s ] %s\n" "NOK" "$t"
            nfail=$((nfail+1))
        fi
    done
    
    printf "[==========] %d test cases ran.\n" "$#"
    printf "[  PASSED  ] %d tests.\n" $npass
    printf "[  FAILED  ] %d tests.\n" $nfail

    if [ $nfail -ne 0 ]
    then
        exit 1
    fi
}

. "$(dirname "$0")/false.sh"
. "$(dirname "$0")/true.sh"
. "$(dirname "$0")/rm.sh"
. "$(dirname "$0")/cat.sh"
. "$(dirname "$0")/sort.sh"

setup
run_tests \
    false_should_return_zero \
    true_should_return_nonzero \
    rm_backup_file \
    rm_should_handle_dash \
    rm_should_handle_directory \
    rm_should_follow_symlinks \
    cat_should_handle_one_file \
    cat_should_handle_two_files \
    cat_should_handle_stdin \
    cat_should_handle_u_flag \
    cat_should_handle_dash \
    sort_should_handle_one_file \
    sort_should_handle_two_files \
    sort_should_handle_stdin \
    sort_should_handle_o_flag \
    sort_should_handle_dash
